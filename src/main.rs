use axum::{body::Body, http::Request, response::Html, routing::get, Extension, Router};
use axum_extra::routing::SpaRouter;
use axum_macros::debug_handler;
use std::net::{IpAddr, Ipv6Addr, SocketAddr};
use sycamore::prelude::*;
use tower::ServiceBuilder;
use tower_http::trace::TraceLayer;

#[tokio::main]
async fn main() {
    // Setup logging & RUST_LOG
    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "debug,hyper=info,mio=info");
    }
    // Enable console logging
    tracing_subscriber::fmt::init();

    // Read the index.html in our static dir
    let index_html = tokio::fs::read_to_string(format!("{}/index.html", "./dist"))
        .await
        .expect("Reading index.html failed");

    let app = Router::new()
        .merge(SpaRouter::new("/assets", "./dist"))
        .fallback(get(render).layer(Extension(index_html)))
        .layer(ServiceBuilder::new().layer(TraceLayer::new_for_http()));

    let sock_addr = SocketAddr::from((IpAddr::V6(Ipv6Addr::LOCALHOST), 8080));

    log::info!("Listening on http://{}", sock_addr);

    axum::Server::bind(&sock_addr)
        .serve(app.into_make_service())
        .await
        .expect("Unable to start server");
}

#[debug_handler]
async fn render(index_html: Extension<String>, req: Request<Body>) -> Html<String> {
    let rendered = sycamore::render_to_string(|ctx| {
        view! { ctx,
            frontend::App(Some(req.uri().to_string()))
        }
    });

    Html::from(index_html.replace("%body", &rendered))
}
