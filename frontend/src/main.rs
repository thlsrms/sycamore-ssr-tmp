use sycamore::prelude::*;

fn main() {
    console_error_panic_hook::set_once();
    wasm_logger::init(wasm_logger::Config::new(log::Level::Trace));

    sycamore::hydrate(|ctx| {
        view! { ctx,
            frontend::App(None)
        }
    });
}
