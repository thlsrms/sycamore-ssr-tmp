use sycamore::prelude::*;
use sycamore_router::{HistoryIntegration, Route, Router, StaticRouter};

#[derive(Route, Clone)]
pub enum Routes {
    #[to("/")]
    Index,
    #[to("/two")]
    Two,
    #[to("/three")]
    Three,
    #[not_found]
    NotFound,
}

#[component]
pub fn App<G: Html>(ctx: Scope, route: Option<String>) -> View<G> {
    match route {
        Some(path) => view! { ctx,
            StaticRouter {
                view: switch,
                route: Routes::match_path(&Routes::default(), &path),
            }
        },

        None => view! { ctx,
            Router {
                view: switch,
                integration: HistoryIntegration::new(),
            }
        },
    }
}

fn switch<'a, G: Html>(ctx: Scope<'a>, route: &'a ReadSignal<Routes>) -> View<G> {
    view! { ctx,
        div {
            Navbar()
            div {
                (match route.get().as_ref() {
                    Routes::Index => view! { ctx, Home() },
                    Routes::Two => view! { ctx, Two() },
                    Routes::Three => view! { ctx, Three() },
                    Routes::NotFound => view! { ctx, h1 {"404"} },
                })
            }
        }
    }
}

// ---------- Components -------------- //

#[component]
pub fn Home<G: Html>(ctx: Scope) -> View<G> {
    view! { ctx,
        div {
            h1 { "Home page" }
            h2 { "Hello World" }
        }
    }
}

#[component]
pub fn Two<G: Html>(ctx: Scope) -> View<G> {
    view! { ctx,
        div {
            h1 { "Page Two" }
            h2 { "Hello World" }
        }
    }
}

#[component]
pub fn Three<G: Html>(ctx: Scope) -> View<G> {
    view! { ctx,
        div {
            h1 { "Page Three" }
            h2 { "Hello World" }
        }
    }
}

#[component]
pub fn Navbar<G: Html>(ctx: Scope) -> View<G> {
    view! { ctx,
        nav {
            ul {
                li {
                    a(href="/") { "Home" }
                }
                li {
                    a(href="/two") { "Two" }
                }
                li {
                    a(href="/three") { "Three" }
                }
            }
        }
    }
}
